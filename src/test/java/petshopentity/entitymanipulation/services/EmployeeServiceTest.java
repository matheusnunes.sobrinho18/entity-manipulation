package petshopentity.entitymanipulation.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import petshopentity.entitymanipulation.ConfigTest;
import petshopentity.entitymanipulation.dtos.EmployeeDto;
import petshopentity.entitymanipulation.models.EmployeeModel;
import petshopentity.entitymanipulation.repositories.EmployeeRepository;

import java.util.Optional;

@DisplayName("EmployeeServiceTest")
public class EmployeeServiceTest extends ConfigTest {

    @MockBean
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployeeService employeeService;

    @Test
    @DisplayName("Should save an employee in database")
    public void saveTest(){
        EmployeeModel employeeModelWithId = createEmployeeModel(1L);
        EmployeeModel employeeModelWithoutId = createEmployeeModel();

        Mockito.when(employeeRepository.save(ArgumentMatchers.eq(employeeModelWithoutId))).thenReturn(employeeModelWithId);

        employeeService.save(createEmployeeDto());

        Mockito.verify(employeeRepository, Mockito.times(1)).save(ArgumentMatchers.any(EmployeeModel.class));
    }

    @Test
    @DisplayName("Should search an employee in database")
    public void findByIdTest(){
        Long id = 1L;
        Optional<EmployeeModel> employeeModelOptionalMock = Optional.of(createEmployeeModel(id));

        Mockito.when(employeeRepository.findById(id)).thenReturn(employeeModelOptionalMock);

        Optional<EmployeeModel> employeeModelOptional = employeeService.findById(1L);

        Mockito.verify(employeeRepository, Mockito.times(1)).findById(ArgumentMatchers.any(Long.class));
        Assertions.assertEquals(1L, employeeModelOptional.get().getId());
    }

    private EmployeeModel createEmployeeModel(Long id){
        EmployeeModel employeeModel = Mockito.mock(EmployeeModel.class);
        Mockito.when(employeeModel.getId()).thenReturn(1L);
        return employeeModel;
    }

    private EmployeeModel createEmployeeModel(){
        EmployeeModel employeeModel = Mockito.mock(EmployeeModel.class);

        return employeeModel;
    }

    private EmployeeDto createEmployeeDto(){
        EmployeeDto employeeDto = Mockito.mock(EmployeeDto.class);

        return employeeDto;
    }

}
