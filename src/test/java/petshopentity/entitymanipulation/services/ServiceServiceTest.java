package petshopentity.entitymanipulation.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import petshopentity.entitymanipulation.ConfigTest;
import petshopentity.entitymanipulation.dtos.ServiceDto;
import petshopentity.entitymanipulation.models.ServiceModel;
import petshopentity.entitymanipulation.repositories.ServiceRepository;

@DisplayName("ServiceServiceTest")
public class ServiceServiceTest extends ConfigTest {

    @MockBean
    private ServiceRepository serviceRepository;

    @Autowired
    private ServiceService serviceService;

    @Test
    @DisplayName("Should save an service in database")
    public void saveTest(){
        ServiceModel serviceModelWithId = createServiceModel(1L);
        ServiceModel serviceModelWithoutId = createServiceModel();

        Mockito.when(serviceRepository.save(ArgumentMatchers.eq(serviceModelWithoutId))).thenReturn(serviceModelWithId);

        serviceService.save(createServiceDto());

        Mockito.verify(serviceRepository, Mockito.times(1)).save(ArgumentMatchers.any(ServiceModel.class));
    }

    public ServiceModel createServiceModel(Long id){
        ServiceModel serviceModel = Mockito.mock(ServiceModel.class);
        Mockito.when(serviceModel.getId()).thenReturn(1L);
        return serviceModel;
    }

    public ServiceModel createServiceModel(){
        ServiceModel serviceModel = Mockito.mock(ServiceModel.class);

        return serviceModel;
    }

    private ServiceDto createServiceDto() {
        ServiceDto serviceDto = Mockito.mock(ServiceDto.class);

        return serviceDto;
    }

}
