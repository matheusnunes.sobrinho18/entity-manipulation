package petshopentity.entitymanipulation.services;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petshopentity.entitymanipulation.dtos.ClientDto;
import petshopentity.entitymanipulation.models.AnimalModel;
import petshopentity.entitymanipulation.models.ClientModel;
import petshopentity.entitymanipulation.repositories.ClientRepository;

import java.util.List;
import java.util.Optional;

@Service
@Builder
@AllArgsConstructor
public class ClientService {

    final ClientRepository clientRepository;

    @Transactional
    public ClientModel save(ClientDto clientDto){
        ClientModel clientModel = new ClientModel();
        BeanUtils.copyProperties(clientDto, clientModel);
        clientModel.setAnimals(null);
        return clientRepository.save(clientModel);
    }

    public Optional<ClientModel> findById(Long clientId) {
        return clientRepository.findById(clientId);
    }

    public List<ClientModel> findAll() {
        return clientRepository.findAll();
    }

    public ClientModel update(Long id, List<AnimalModel> animalModelList, ClientDto clientDto){
        ClientModel clientModel = new ClientModel();
        BeanUtils.copyProperties(clientDto, clientModel);
        clientModel.setId(id);
        clientModel.setAnimals(animalModelList);
        return clientRepository.save(clientModel);
    }

    @Transactional
    public void delete(ClientModel clientModel) {
        clientRepository.delete(clientModel);
    }
}
