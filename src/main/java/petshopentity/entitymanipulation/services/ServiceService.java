package petshopentity.entitymanipulation.services;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petshopentity.entitymanipulation.dtos.ServiceDto;
import petshopentity.entitymanipulation.models.ServiceModel;
import petshopentity.entitymanipulation.repositories.ServiceRepository;

import java.util.List;
import java.util.Optional;

@Service
@Builder
@AllArgsConstructor
public class ServiceService {

    final ServiceRepository serviceRepository;

    @Transactional
    public ServiceModel save(ServiceDto serviceDto){
        ServiceModel serviceModel = new ServiceModel();
        BeanUtils.copyProperties(serviceDto, serviceModel);
        return serviceRepository.save(serviceModel);
    }

    public List<ServiceModel> finAll() {
        return serviceRepository.findAll();
    }

    public Optional<ServiceModel> findById(Long id){
        return serviceRepository.findById(id);
    }

    @Transactional
    public void delete(ServiceModel serviceModel){
        serviceRepository.delete(serviceModel);
    }

    public ServiceModel update(Long id, ServiceDto serviceDto){
        ServiceModel serviceModel = new ServiceModel();
        BeanUtils.copyProperties(serviceDto, serviceModel);
        serviceModel.setId(id);
        return serviceRepository.save(serviceModel);
    }

}
