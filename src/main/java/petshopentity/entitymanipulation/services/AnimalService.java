package petshopentity.entitymanipulation.services;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petshopentity.entitymanipulation.dtos.AnimalDto;
import petshopentity.entitymanipulation.dtos.AnimalResponseDto;
import petshopentity.entitymanipulation.models.AnimalModel;
import petshopentity.entitymanipulation.models.ClientModel;
import petshopentity.entitymanipulation.repositories.AnimalRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Builder
@AllArgsConstructor
public class AnimalService {

    final AnimalRepository animalRepository;

    @Transactional
    public AnimalResponseDto save(AnimalDto animalDto, ClientModel clientModel){
        AnimalModel animalModel = new AnimalModel();
        BeanUtils.copyProperties(animalDto, animalModel);
        animalModel.setClient(clientModel);
        animalRepository.save(animalModel);

        return new AnimalResponseDto(animalModel);
    }

    public List<AnimalResponseDto> findAll(){
        List<AnimalModel> animalModelList = animalRepository.findAll();
        List<AnimalResponseDto> animalResponseDtoList = new ArrayList<>();

        for(AnimalModel animalModel : animalModelList){
            AnimalResponseDto animalResponseDto = new AnimalResponseDto(animalModel);
            animalResponseDtoList.add(animalResponseDto);
        }

        return animalResponseDtoList;
    }

    public AnimalResponseDto findById(Long id) {
        Optional<AnimalModel> animalModelOptional = animalRepository.findById(id);

        if(!animalModelOptional.isPresent()){
            return null;
        }

        return new AnimalResponseDto(animalModelOptional.get());
    }

    public List<AnimalModel> findByClient(ClientModel clientModel) {
        return animalRepository.findByClient(clientModel);
    }

    public AnimalResponseDto update(Long id, AnimalDto animalDto, ClientModel clientModel) {
        AnimalModel animalModel = new AnimalModel();
        BeanUtils.copyProperties(animalDto, animalModel);
        animalModel.setId(id);
        animalModel.setClient(clientModel);
        animalRepository.save(animalModel);
        return new AnimalResponseDto(animalModel);
    }

    @Transactional
    public void delete(AnimalResponseDto animalResponseDto, ClientModel clientModel) {
        AnimalModel animalModel = new AnimalModel();
        BeanUtils.copyProperties(animalResponseDto, animalModel);
        animalModel.setClient(clientModel);
        animalRepository.delete(animalModel);
    }

}
