package petshopentity.entitymanipulation.services;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import petshopentity.entitymanipulation.dtos.EmployeeDto;
import petshopentity.entitymanipulation.models.EmployeeModel;
import petshopentity.entitymanipulation.repositories.EmployeeRepository;

import java.util.List;
import java.util.Optional;

@Service
@Builder
@AllArgsConstructor
public class EmployeeService {

    final EmployeeRepository employeeRepository;

    @Transactional
    public EmployeeModel save(EmployeeDto employeeDto){
        EmployeeModel employeeModel = new EmployeeModel();
        BeanUtils.copyProperties(employeeDto, employeeModel);
        return employeeRepository.save(employeeModel);
    }

    public List<EmployeeModel> findAll() {
        return employeeRepository.findAll();
    }

    public Optional<EmployeeModel> findById(Long id){
        return employeeRepository.findById(id);
    }

    public EmployeeModel update(Long id, EmployeeDto employeeDto) {
        EmployeeModel employeeModel = new EmployeeModel();
        BeanUtils.copyProperties(employeeDto, employeeModel);
        employeeModel.setId(id);
        return employeeRepository.save(employeeModel);
    }

    @Transactional
    public void delete(EmployeeModel employeeModel) {
        employeeRepository.delete(employeeModel);
    }
}
