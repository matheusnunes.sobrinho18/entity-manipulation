package petshopentity.entitymanipulation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import petshopentity.entitymanipulation.models.ServiceModel;

@Repository
public interface ServiceRepository extends JpaRepository<ServiceModel, Long> {



}
