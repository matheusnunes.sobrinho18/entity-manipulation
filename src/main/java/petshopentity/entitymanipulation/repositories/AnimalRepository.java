package petshopentity.entitymanipulation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import petshopentity.entitymanipulation.models.AnimalModel;
import petshopentity.entitymanipulation.models.ClientModel;

import java.util.List;

@Repository
public interface AnimalRepository extends JpaRepository<AnimalModel, Long> {

    List<AnimalModel> findByClient(ClientModel clientModel);

}
