package petshopentity.entitymanipulation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import petshopentity.entitymanipulation.models.ClientModel;

@Repository
public interface ClientRepository extends JpaRepository<ClientModel, Long> {

}
