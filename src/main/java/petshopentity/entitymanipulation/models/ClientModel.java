package petshopentity.entitymanipulation.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "TB_CLIENT")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(unique = true)
    private String email;

    @Column(nullable = false, unique = true, length = 13)
    private String tel;

    @Column(nullable = false, length = 10)
    private String zipCode;

    @OneToMany()
    @JoinColumn(name = "client_id")
    @JsonManagedReference
    private List<AnimalModel> animals;

}
