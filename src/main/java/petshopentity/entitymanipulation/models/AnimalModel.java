package petshopentity.entitymanipulation.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import petshopentity.entitymanipulation.enums.AnimalSize;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TB_ANIMAL")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnimalModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String specie;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private AnimalSize animalSize;

    @ManyToOne()
    @JoinColumn(name = "client_id")
    @JsonBackReference
    private ClientModel client;
}
