package petshopentity.entitymanipulation.models;

import lombok.*;
import petshopentity.entitymanipulation.enums.EmployeeFunction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TB_EMPLOYEE")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private EmployeeFunction function;
}
