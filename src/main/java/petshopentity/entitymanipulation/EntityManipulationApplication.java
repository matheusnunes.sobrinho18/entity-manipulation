package petshopentity.entitymanipulation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class EntityManipulationApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntityManipulationApplication.class, args);
	}

	@GetMapping("/")
	public String index(){
		return "Tudo certo!!";
	}

}
