package petshopentity.entitymanipulation.dtos;

import lombok.*;
import petshopentity.entitymanipulation.enums.AnimalSize;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AnimalDto {

    @NotBlank
    private String name;

    @NotBlank
    private String specie;

    @NotNull
    private AnimalSize animalSize;

    @NotNull
    private Long clientId;
}
