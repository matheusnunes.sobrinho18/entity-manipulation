package petshopentity.entitymanipulation.dtos;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ServiceDto {

    @NotBlank
    private String name;

    @NotNull
    private Double price;

}
