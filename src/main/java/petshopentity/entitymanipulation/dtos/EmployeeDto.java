package petshopentity.entitymanipulation.dtos;

import lombok.Getter;
import lombok.Setter;
import petshopentity.entitymanipulation.enums.EmployeeFunction;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class EmployeeDto {

    @NotBlank
    private String name;

    @NotNull
    private EmployeeFunction function;
}
