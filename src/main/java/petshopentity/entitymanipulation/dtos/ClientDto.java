package petshopentity.entitymanipulation.dtos;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class ClientDto {

    @NotBlank
    private String name;

    @NotBlank
    private String email;

    @NotBlank
    @Length(max = 13)
    private String tel;

    @NotBlank
    @Length(max = 10)
    private String zipCode;

}
