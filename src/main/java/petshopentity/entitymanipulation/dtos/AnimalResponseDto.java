package petshopentity.entitymanipulation.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import petshopentity.entitymanipulation.enums.AnimalSize;
import petshopentity.entitymanipulation.models.AnimalModel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AnimalResponseDto {

    @NotNull
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String specie;

    @NotNull
    private AnimalSize animalSize;

    @NotNull
    private Long clientId;

    public AnimalResponseDto(AnimalModel animalModel) {
        BeanUtils.copyProperties(animalModel, this);
        this.clientId = animalModel.getClient().getId();
    }
}
