package petshopentity.entitymanipulation.enums;

public enum EmployeeFunction {
    SECRETARY,
    VET,
    PET_HAIRDRESSER,
    PET_GROOMER
}
