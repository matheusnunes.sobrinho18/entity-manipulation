package petshopentity.entitymanipulation.enums;

public enum AnimalSize {
    Small,
    Medium,
    Large
}
