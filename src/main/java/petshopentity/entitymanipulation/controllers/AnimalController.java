package petshopentity.entitymanipulation.controllers;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petshopentity.entitymanipulation.dtos.AnimalDto;
import petshopentity.entitymanipulation.dtos.AnimalResponseDto;
import petshopentity.entitymanipulation.models.ClientModel;
import petshopentity.entitymanipulation.services.AnimalService;
import petshopentity.entitymanipulation.services.ClientService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Builder
@AllArgsConstructor
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/animal")
public class AnimalController {

    final AnimalService animalService;
    final ClientService clientService;

    @PostMapping
    public ResponseEntity<Object> saveAnimal(@RequestBody @Valid AnimalDto animalDto){
        Optional<ClientModel> clientModelOptional = clientService.findById(animalDto.getClientId());

        if (!clientModelOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Client not found");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(animalService.save(animalDto, clientModelOptional.get()));
    }

    @GetMapping
    public ResponseEntity<List<AnimalResponseDto>> findAllClients(){
        return ResponseEntity.status(HttpStatus.OK).body(animalService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getOneAnimal(@PathVariable(value = "id") Long id){
        AnimalResponseDto animalResponseDto = animalService.findById(id);

        if (animalResponseDto == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Animal not found");
        }

        return ResponseEntity.status(HttpStatus.FOUND).body(animalResponseDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateAnimal(@PathVariable(value = "id") Long id, @RequestBody @Valid AnimalDto animalDto){
        AnimalResponseDto animalResponseDto = animalService.findById(id);
        Optional<ClientModel> clientModelOptional = clientService.findById(animalDto.getClientId());

        if (animalResponseDto == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Animal not found");
        }

        if (!clientModelOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Client not found");
        }

        return ResponseEntity.status(HttpStatus.OK).body(animalService.update(id, animalDto, clientModelOptional.get()));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteAnimal(@PathVariable(value = "id") Long id){
        AnimalResponseDto animalResponseDto = animalService.findById(id);

        if (animalResponseDto == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Animal not found");
        }

        Optional<ClientModel> clientModel = clientService.findById(animalResponseDto.getClientId());

        animalService.delete(animalResponseDto, clientModel.get());
        return ResponseEntity.status(HttpStatus.OK).body("Animal deleted successfully");
    }

}
