package petshopentity.entitymanipulation.controllers;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petshopentity.entitymanipulation.dtos.EmployeeDto;
import petshopentity.entitymanipulation.models.EmployeeModel;
import petshopentity.entitymanipulation.services.EmployeeService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Builder
@AllArgsConstructor
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/employee")
public class EmployeeController {

    final EmployeeService employeeService;

    @PostMapping
    public ResponseEntity<Object> saveEmployee(@RequestBody @Valid EmployeeDto employeeDto){
        return ResponseEntity.status(HttpStatus.CREATED).body(employeeService.save(employeeDto));
    }

    @GetMapping
    public ResponseEntity<List<EmployeeModel>> getAllEmployees(){
        return ResponseEntity.status(HttpStatus.OK).body(employeeService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getOneEmployee(@PathVariable(value = "id") Long id){
        Optional<EmployeeModel> employeeModelOptional = employeeService.findById(id);

        if (!employeeModelOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Employee not found");
        }

        return ResponseEntity.status(HttpStatus.FOUND).body(employeeModelOptional.get());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteEmployee(@PathVariable(value = "id") Long id){
        Optional<EmployeeModel> employeeModelOptional = employeeService.findById(id);

        if (!employeeModelOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Employee not found");
        }

        employeeService.delete(employeeModelOptional.get());
        return ResponseEntity.status(HttpStatus.OK).body("Employee deleted successfully");
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateEmployee(@PathVariable(value = "id") Long id, @RequestBody @Valid EmployeeDto employeeDto){
        Optional<EmployeeModel> employeeModelOptional = employeeService.findById(id);

        if (!employeeModelOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Employee not found");
        }

        return ResponseEntity.status(HttpStatus.OK).body(employeeService.update(id, employeeDto));
    }

}
