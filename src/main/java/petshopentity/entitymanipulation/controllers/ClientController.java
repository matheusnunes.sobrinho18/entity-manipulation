package petshopentity.entitymanipulation.controllers;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petshopentity.entitymanipulation.dtos.ClientDto;
import petshopentity.entitymanipulation.models.AnimalModel;
import petshopentity.entitymanipulation.models.ClientModel;
import petshopentity.entitymanipulation.models.EmployeeModel;
import petshopentity.entitymanipulation.services.AnimalService;
import petshopentity.entitymanipulation.services.ClientService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Builder
@AllArgsConstructor
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/client")
public class ClientController {

    final ClientService clientService;
    final AnimalService animalService;

    @PostMapping
    public ResponseEntity<Object> saveClient(@RequestBody @Valid ClientDto clientDto){
        return ResponseEntity.status(HttpStatus.CREATED).body(clientService.save(clientDto));
    }

    @GetMapping
    public ResponseEntity<List<ClientModel>> findAllClients(){
        return ResponseEntity.status(HttpStatus.OK).body(clientService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getOneClient(@PathVariable(value = "id") Long id){
        Optional<ClientModel> clientModelOptional = clientService.findById(id);

        if (!clientModelOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Client not found");
        }

        return ResponseEntity.status(HttpStatus.FOUND).body(clientModelOptional.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateClient(@PathVariable(value = "id") Long id, @RequestBody @Valid ClientDto clientDto){
        Optional<ClientModel> clientModelOptional = clientService.findById(id);

        if (!clientModelOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Client not found");
        }

        List<AnimalModel> animalModelList = animalService.findByClient(clientModelOptional.get());

        return ResponseEntity.status(HttpStatus.OK).body(clientService.update(id, animalModelList, clientDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteClient(@PathVariable(value = "id") Long id){
        Optional<ClientModel> clientModelOptional = clientService.findById(id);

        if (!clientModelOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Client not found");
        }

        clientService.delete(clientModelOptional.get());
        return ResponseEntity.status(HttpStatus.OK).body("Client deleted successfully");
    }

}
