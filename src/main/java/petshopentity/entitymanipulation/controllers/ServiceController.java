package petshopentity.entitymanipulation.controllers;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import petshopentity.entitymanipulation.dtos.ServiceDto;
import petshopentity.entitymanipulation.models.ServiceModel;
import petshopentity.entitymanipulation.services.ServiceService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Builder
@AllArgsConstructor
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/service")
public class ServiceController {

    final ServiceService serviceService;

    @PostMapping
    public ResponseEntity<Object> saveService(@RequestBody @Valid ServiceDto serviceDto){
        return ResponseEntity.status(HttpStatus.CREATED).body(serviceService.save(serviceDto));
    }

    @GetMapping
    public ResponseEntity<List<ServiceModel>> getAllServices(){
        return ResponseEntity.status(HttpStatus.OK).body(serviceService.finAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getOneService(@PathVariable(value = "id") Long id){
        Optional<ServiceModel> serviceModelOptional = serviceService.findById(id);

        if (!serviceModelOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Service not found");
        }

        return ResponseEntity.status(HttpStatus.FOUND).body(serviceModelOptional.get());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteService(@PathVariable(value = "id") Long id){
        Optional<ServiceModel> serviceModelOptional = serviceService.findById(id);

        if (!serviceModelOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Service not found");
        }

        serviceService.delete(serviceModelOptional.get());
        return ResponseEntity.status(HttpStatus.OK).body("Service deleted successfully");
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateService(@PathVariable(value = "id") Long id, @RequestBody @Valid ServiceDto serviceDto){

        Optional<ServiceModel> serviceModelOptional = serviceService.findById(id);

        if (!serviceModelOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Service not found");
        }

        return ResponseEntity.status(HttpStatus.OK).body(serviceService.update(id, serviceDto));
    }

}
