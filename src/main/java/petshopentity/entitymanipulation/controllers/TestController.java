package petshopentity.entitymanipulation.controllers;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import petshopentity.entitymanipulation.models.AnimalModel;
import petshopentity.entitymanipulation.models.ClientModel;
import petshopentity.entitymanipulation.services.AnimalService;
import petshopentity.entitymanipulation.services.ClientService;

import java.util.List;
import java.util.Optional;

@Builder
@AllArgsConstructor
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/test")
public class TestController {

    final ClientService clientService;
    final AnimalService animalService;

    @GetMapping
    public ResponseEntity<List<AnimalModel>> getAllAnimalsByClient(){
        Long id = Long.valueOf(1);
        Optional<ClientModel> clientModelOptional = clientService.findById(id);
        ClientModel clientModel = clientModelOptional.get();

        return ResponseEntity.status(HttpStatus.OK).body(animalService.findByClient(clientModel));
    }
}
