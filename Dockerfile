FROM openjdk:17

COPY target/entity-manipulation-0.0.1-SNAPSHOT.jar entity-manipulation.jar
ENTRYPOINT ["java","-jar","/entity-manipulation.jar"]


# Gerar jar pulando testes: mvn package -DskipTests 
#Dps constrói a imagem: docker build --tag={nome-da-imagem} .
